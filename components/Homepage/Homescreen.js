import {
  FlatList,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import animalsImage from "../../assets/groups/animals.jpg";
import foodImage from "../../assets/groups/food.avif";
import mindfulnessImage from "../../assets/groups/mindfulness.jpg";
import relaxImage from "../../assets/groups/relax.jpg";
export default function Homescreen({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainView}>
        <FlatList
          data={[
            { key: "Animals", imgSrc: animalsImage, route: "Animals" },
            { key: "Food", imgSrc: foodImage, route: "Food" },
            { key: "Relax", imgSrc: relaxImage, route: "Food" },
            { key: "Mindfulness", imgSrc: mindfulnessImage, route: "Food" },
          ]}
          contentContainerStyle={{ gap: 20 }}
          columnWrapperStyle={{ gap: 20 }}
          numColumns={2}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => navigation.navigate(item.route)}>
              <View style={styles.groupViewMember}>
                <Image style={styles.groupImageMember} source={item.imgSrc} />
                <Text style={styles.groupTextMember}>{item.key}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
  mainView: {
    padding: 30,
    position: "absolute",
  },
  groupViewMember: {
    backgroundColor: "black",
    alignItems: "center",
  },
  groupImageMember: {
    width: 140,
    height: 160,
  },
  groupTextMember: {
    fontSize: 25,
    color: "white",
  },
});
