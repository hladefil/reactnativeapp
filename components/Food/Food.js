import { SafeAreaView, StatusBar, StyleSheet, View } from "react-native";
import Carousel from "react-native-snap-carousel";
import CoffeCard from "./Coffe/CoffeCard";

const coffeeData = [
  {
    id: 1,
    name: "Cappucino",
    volume: "110 ml",
    stars: "4.3",
    image: "4.3",
    desc: "fakt popici kafe bracho",
  },
  {
    id: 1,
    name: "Cappucino",
    volume: "110 ml",
    stars: "4.3",
    image: "4.3",
    desc: "fakt popici kafe bracho",
  },
  {
    id: 1,
    name: "Cappucino",
    volume: "110 ml",
    stars: "4.3",
    image: "4.3",
    desc: "fakt popici kafe bracho",
  },
];
export default function Food() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainView}></View>

      <Carousel
        data={coffeeData}
        renderItem={({ item }) => <CoffeCard item={item} />}
        containerCustomStyle={{ overflow: "visible" }}
        firstItem={1}
        inactiveSlideOpacity={0.75}
        inactiveSlideScale={0.77}
        sliderWidth={400}
        itemWidth={260}
        slideStyle={{ display: "flex", alignItems: "center" }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
  mainView: {
    padding: 30,
    position: "absolute",
  },
});
