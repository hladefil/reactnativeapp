import React, { useState } from "react";
import { View, StyleSheet, Dimensions, Text, Image } from "react-native";
import { StatusBar } from "expo-status-bar";
import SwipeGesture from "react-native-swipe-gestures";
import { animalsData } from "../../data/AnimalsData";
import { Button, Card, Icon } from "react-native-elements";
import Animal from "../Animals/Animal";

const SCREEN_WIDTH = Dimensions.get("window").width;

const AnimalListNavigator = () => {
  const [currentAnimal, setCurrentAnimal] = useState(0);

  const onSwipeRight = () => {
    if (currentAnimal === 0) setCurrentAnimal(animalsData.length - 1);
    else setCurrentAnimal(currentAnimal - 1);
  };

  const onSwipeLeft = () => {
    if (currentAnimal === animalsData.length - 1) setCurrentAnimal(0);
    else setCurrentAnimal(currentAnimal + 1);
  };

  return (
    <SwipeGesture
      onSwipeLeft={onSwipeLeft}
      onSwipeRight={onSwipeRight}
      style={styles.swipeContainer}
      config={{
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80,
      }}
    >
      <View style={styles.container}>
        <Animal currentAnimal={currentAnimal} />
      </View>
    </SwipeGesture>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: "black",
    width: "80%",
  },
  swipeContainer: {
    flex: 1,
    // backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
  },
});
export default AnimalListNavigator;
