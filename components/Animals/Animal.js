import React, { useEffect, useRef, useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { animalsData } from "../../data/AnimalsData";
import { Button, Card } from "react-native-elements";
import * as Progress from "react-native-progress";
import ConfettiCannon from "react-native-confetti-cannon";
const Animal = ({ currentAnimal, isFull }) => {
  const [progress, setProgress] = useState(0);
  const [isAnimalFedEnough, setIsAnimalFedEnough] = useState(false);
  const [isHungry, setIsHungry] = useState(animalsData[currentAnimal].isHungry);
  const explosion = useRef();

  useEffect(() => {
    if (!isFull) return;
    setIsAnimalFedEnough(true);
    const timer = setInterval(() => {
      // Increment progress by a certain amount
      setProgress((prevProgress) => {
        const newProgress = prevProgress + 0.25;
        if (newProgress >= 1) {
          // Call handleLoadingEnd function when progress reaches 1
          handleLoadingEnd();
          clearInterval(timer); // Stop the timer
          return 1;
        }
        return newProgress;
      });
    }, 500); // Adjust the interval as needed
    setProgress(0);
    return () => clearInterval(timer); // Clear the interval when component unmounts
  }, [isFull]);

  const handleLoadingEnd = () => {
    setIsAnimalFedEnough(false);
    animalsData[currentAnimal].isFull = false;
    console.log("KONČÍME");
  };

  const shootCannon = () => {
    explosion.current && explosion.current.start();
  };

  const handlePress = () => {
    shootCannon();
    setIsHungry(false);
    // const updatedAnimals = [...animals];
    // let currentAnimalFullFactor = animals[currentAnimalIndex].fullFactor - 1
    // if (currentAnimalFullFactor === -1) {
    //     return
    // }
    // updatedAnimals[currentAnimalIndex] = {
    //     ...updatedAnimals[currentAnimalIndex],
    //     isHungry: false,
    //     fullFactor: currentAnimalFullFactor,
    //     isFull: currentAnimalFullFactor <= 0
    // };
    // setAnimals(updatedAnimals);
  };
  return (
    <View style={styles.viewStyle}>
      <Card containerStyle={styles.mainCard}>
        {isAnimalFedEnough && (
          <Progress.Circle
            thickness={10}
            showsText
            fill={"#FFFFFF"}
            style={styles.progressCircle}
            progress={progress}
            formatText={(progress) => `${Math.round(progress * 100)}%`}
            color={"#009E60"}
            size={300}
            animated
            indeterminateAnimationDuration={100000}
          />
        )}
        <Card.Title>{animalsData[currentAnimal].name}</Card.Title>
        <Card.Divider />
        <View style={{ justifyContent: "center", alignSelf: "center" }}>
          <Card.Image
            //  loadingIndicatorSource={}
            blurRadius={isHungry ? 10 : 0}
            style={{ width: 200, height: 200, alignSelf: "center" }}
            source={animalsData[currentAnimal].imageURL}
          />
        </View>
        <View
          style={{ justifyContent: "center", alignItems: "center", padding: 4 }}
        >
          <Text style={{ marginBottom: 10 }}>
            Favourite food: {animalsData[currentAnimal].favouriteFood}
          </Text>
          <Text>Typical quote: {animalsData[currentAnimal].quote}</Text>
        </View>
      </Card>
      <Button
        disabled={isAnimalFedEnough}
        buttonStyle={{
          marginTop: 10,
          backgroundColor: "black",
          width: "100%",
          alignSelf: "center",
          borderRadius: "10",
        }}
        title="🦴 Feed now"
        titleStyle={{
          color: "white",
        }}
        onPress={handlePress}
      />
      <ConfettiCannon
        count={200}
        origin={{ x: 150, y: -1500 }}
        autoStart={false}
        ref={explosion}
        fadeOut
      />
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    alignItems: "center",
  },
  mainCard: {
    borderTopStartRadius: "40",
    borderTopEndRadius: "40",
  },
  progressCircle: {
    position: "absolute",
    zIndex: "5",
    alignSelf: "center",
  },
});
export default Animal;
