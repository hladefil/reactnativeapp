import Gremlin from "../assets/animals/gremlin.jpg";
import Ckus from "../assets/animals/ckus.jpg";
import Tomas from "../assets/animals/tomas.jpg";
import Lichna from "../assets/animals/lichna.jpg";
import Kosic from "../assets/animals/kosic.jpg";
import Richard from "../assets/animals/richard.jpg";

export const animalsData = [
  {
    name: "Gremlin",
    favouriteFood: "🍔",
    quote: "Phaaa",
    imageURL: Gremlin,
  },
  {
    name: "Richard",
    favouriteFood: "🍟",
    quote: "eHeBleEeeh",
    imageURL: Richard,
  },
  {
    name: "Tomas",
    favouriteFood: "🥛",
    quote: "Mám vpiči",
    imageURL: Tomas,
  },
  {
    name: "Čkus",
    favouriteFood: "🍕",
    quote: "Jsem obrovský",
    imageURL: Ckus,
  },
  {
    name: "Kosič",
    favouriteFood: "🥚",
    quote: "Polib si prdel",
    imageURL: Kosic,
  },
  {
    name: "Lichna",
    favouriteFood: "🍟",
    quote: "Méďu béďuu",
    imageURL: Lichna,
  },
];
