import { StatusBar } from "react-native";
import Homescreen from "./components/Homepage/Homescreen";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AnimalListNavigator from "./components/Animals/AnimalListNavigator";
import Food from "./components/Food/Food";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor="black" barStyle="light-content" />
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Homescreen}
          options={{
            ...screenOption,
            title: "Home",
          }}
        />
        <Stack.Screen
          name="Animals"
          component={AnimalListNavigator}
          options={{
            ...screenOption,
            title: "Animals",
          }}
        />
        <Stack.Screen
          name="Food"
          component={Food}
          options={{
            ...screenOption,
            title: "Food",
          }}
        />
      </Stack.Navigator>
      {/*<Homepage />*/}
    </NavigationContainer>
  );
}

const screenOption = {
  headerTintColor: "white",
  headerStyle: { backgroundColor: "black" },
};
